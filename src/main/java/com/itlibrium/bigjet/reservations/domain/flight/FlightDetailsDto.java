package com.itlibrium.bigjet.reservations.domain.flight;

import lombok.Value;

import java.time.LocalDate;

@Value
public class FlightDetailsDto {
	LocalDate date;
	String flightNumber;
}
