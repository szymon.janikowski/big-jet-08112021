package com.itlibrium.bigjet.reservations.domain.pax;

import lombok.Value;

@Value
public class PaxDetailsDto {
	String paxType;
	Integer paxNumber;
	Boolean hasInfant;
}
