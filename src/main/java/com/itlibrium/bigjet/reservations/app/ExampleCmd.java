package com.itlibrium.bigjet.reservations.app;

import lombok.Value;

@Value
public class ExampleCmd {
    String param;
}
