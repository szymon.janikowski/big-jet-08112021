package com.itlibrium.bigjet.commons.persistence;

import lombok.Value;

@Value(staticConstructor = "with")
public class PersistenceInfo {
    Long id;
    Long version;
}