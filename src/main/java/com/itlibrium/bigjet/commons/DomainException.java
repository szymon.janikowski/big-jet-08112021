package com.itlibrium.bigjet.commons;

public class DomainException extends RuntimeException{

    public DomainException(String message) {
        super(message);
    }
}
