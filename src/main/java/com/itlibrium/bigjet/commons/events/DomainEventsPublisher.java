package com.itlibrium.bigjet.commons.events;

import io.vavr.collection.List;

public interface DomainEventsPublisher {

    void publish(DomainEvent event);

    default void publish(List<DomainEvent> events) {
        events.forEach(this::publish);
    }
}
