package com.itlibrium.bigjet.commons.events.publisher;

import com.itlibrium.bigjet.commons.events.DomainEvent;
import com.itlibrium.bigjet.commons.events.DomainEventsPublisher;
import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;


@AllArgsConstructor
public class StoreAndForwardDomainEventPublisher implements DomainEventsPublisher {

    private final DomainEventsPublisher eventsPublisher;
    private final EventsStorage eventsStorage;

    @Override
    public void publish(DomainEvent event) {
        eventsStorage.save(event);
    }

    @Scheduled(fixedRate = 3000L)
    @Transactional
    public void publishAllPeriodically() {
        List<DomainEvent> domainEvents = eventsStorage.toPublish();
        domainEvents.forEach(eventsPublisher::publish);
        eventsStorage.published(domainEvents);
    }
}
