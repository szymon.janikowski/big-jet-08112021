package com.itlibrium.bigjet.commons.events.publisher;


import com.itlibrium.bigjet.commons.events.DomainEventsPublisher;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainEventsConfig {

    @Bean
	DomainEventsPublisher domainEvents(ApplicationEventPublisher applicationEventPublisher) {
        return new JustForwardDomainEventPublisher(applicationEventPublisher);
    }
}
