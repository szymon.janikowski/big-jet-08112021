package com.itlibrium.bigjet.commons.events.publisher;

import com.itlibrium.bigjet.commons.events.DomainEvent;
import com.itlibrium.bigjet.commons.events.DomainEventsPublisher;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;


@AllArgsConstructor
public class JustForwardDomainEventPublisher implements DomainEventsPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void publish(DomainEvent event) {
        applicationEventPublisher.publishEvent(event);
    }
}

